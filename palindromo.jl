using Unicode

function palindromo(n)
	n=Unicode.normalize(n, stripmark=true)#para tirar os acentos
	n=Unicode.normalize(n, casefold=true)#para deixar todas as letras minúsculas
	#replace - para substituir pontuações por uma string vazia, o que equivale a removê-las
	n=replace(n, '!' => "")
	n=replace(n, '.' => "")
	n=replace(n, ' ' => "")
	n=replace(n, '?' => "")
	n=replace(n, '-' => "")
	n=replace(n, ',' => "")
	n=replace(n, ';' => "")
	n=replace(n, ':' => "")
	n=replace(n, '/' => "")
	n=replace(n, '(' => "")
	n=replace(n, ')' => "")
	nao=0
	i=1
	
    while i<=length(n)/2
        if n[i]!=n[length(n)-i+1]
           nao=1
        end
	i+=1
    end
	if (nao==1)
		return false	
	else
		return true
	end
end

using Test
function test()
	@test  palindromo("")==true
	@test  palindromo("ovo")==true
	@test  palindromo("MiniEP11")==false
	@test  palindromo("Socorram-me, subi no ônibus em Marrocos!")==true
	@test  palindromo("A mãe te ama.")==true
 	@test  palindromo("Passei em MAC0110!")==false
 	@test  palindromo("1234321")==true
 	@test  palindromo("?l/u.z,a:z)u(l")==true
 	println("Final dos testes.")
end

test()
        
